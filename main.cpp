/*
* Program będący implementacją algorytmu zadania pierwszego z Programowania.
* @author Dawid Dominiak
* @licence MIT
*/

#include <iostream>
#include <cstdlib>
#include <string>
#include <locale>
#include <cmath>
#include <map>

using namespace std; //Używamy przestrzeni nazw std. Nie występują w kodzie żadne kolizje nazw.

/*
* Definicja klasy Person. Zbiór właściwości opisujących osobę i metod, które może wykonać.
*/
class Person
{
private:
	string name; //Właściwość prywatna - imię.
	char gender; //Właściwość prywatna - płeć.
public: //Definicje publicznych metod - opisy przy deklaracjach.
	string sayHello();
	void setName(string name);
	void setGender(char gender);
	char findGenderByName();
	string getVocativeOfSelfName();
};

/*
* Definicja klasy Number. Zbiór właściwości opisujących osobę i metod, które może wykonać.
*/
class Number
{
private:
	int number; //właściwość prywatna - numer.
public: //Definicje publicznych metod - opisy przy deklaracjach.
	int getLength();
	void set(int number);
	int toInt();
	//Definicja operatorów do obsługi strumieni i deklaracja jako zaprzyjaźnione. Dzięki temu uzyskają dostęp do prywatnych właściwości i metod.
	friend ostream & operator<< (ostream &output, const Number &n); //Operator dla strumienia wyjściowego.
	friend istream & operator>> (istream &input, Number &n); //Operator dla strumienia wejściowego.
};

/*
* Definicja z deklaracją funkcji zbierającej dane o użytkowniku ze strumieni wejściowych. Funkcja jest fabryką obiektów klasy Person.
*/
Person fabricatePerson() {
	Person somebody; //Definicja obiektu osoby której dane są wprowadzane. Będzie przechowywać instancję klasy osoby z konkretnymi danymi.
	string name; //Definicja zmiennej nazwy.
	char gender; //Definicja zmiennej płci.

	cout << "Wprowadz imie: "; //Strumien wyjściowy służący do wyświetlenia komendy.
	cin >> name; //Strumień wejściowy przypisujący dane do zmiennej name.
	somebody.setName(name); //Wywołanie metody setName obiektu somebody z argumentem name.
	somebody.setGender(somebody.findGenderByName()); //Pierwsze wywołanie metody setGender z argumentem będącym wynikiem działania metody findGenderByName obiektu somebody
	cout << endl << "Wprowadz plec (wykryto plec: " << (char) toupper(somebody.findGenderByName()) << " - to jest wartosc domyslna) wprowadz inna <M/K>: "; //Strumień wyjściowy - wyświetlenie komendy.
	cin >> gender; //Strumień wejściowy przypisujący dane do zmiennej gender
	somebody.setGender(gender); //Drugie wywołanie metody setGender z argumentem gender.
	return somebody; //Zwrócenie obiektu osoby.
}

/*
* Definicja z deklaracją funkcji tworzącej instancję klasy number z wartością ze strumienia wejściowego. Funkcja jest fabryką obiektów klasy Number.
* @param commandToDisplay {string} - polecenie wyświetlone przez strumień wyjściowy.
*/
Number fabricateNumber(string commandToDisplay) {
	Number number; //Definicja obiektu numeru, którego dane sa wprowadzone.

	cout << commandToDisplay + ": "; //Strumien wyjściowy służący do wyświetlenia komendy.
	cin >> number; //Strumień wejściowy służący przypisujący dane do obiektu number.

	return number; //Zwrócenie obiektu numeru.
}

/*
* Definicja z deklaracją funkcji generującej zgodne z algorytmem opisanym w zadaniu wyjście z obliczonej długości.
* @param length {string} - długość.
*/
string getOutputStringFromLength(int length) {
	switch(length) {
		case 1:
			return "jednocyfrowa";
		case 2:
			return "dwucyfrowa";
	}
	return "inna";
}

/*
* Definicja z deklaracją funkcji która ma zebrać dane użytkownika i wyświetlić odpowiedni komunikat.
*/
void collectPersonDataAndShowInformations() {
	Person somebody; //Definicja obiektu osoby

	somebody = fabricatePerson(); //Wywołanie funkcji fabrykującej obiekt.
	cout << somebody.sayHello() << endl; //Wyświetlenie tego, co zwróci metoda sayHello.
}

/*
* Funkcja dzieli pierwszy argument przez podwojoną wartość drugiego. Wyrzuca wyjątek w przypadku próby dzielenia przez 0
*/
double divideFirstByDoubleSecond(int first, int second) {
	string exception;
	if(second == 0) {
		exception = "Probowano dzielic przez 0";
		throw exception;
	}
	return (double) first/(second * 2);
}

/*
* Definicja z deklaracją funkcji która ma zebrać dwie liczby, pokazać ich długość zgodnie z wzorem ze specyfikacji i podzielić pierwszą liczbę przez dwukrotność drugiej.
*/
void collectTwoNumbersAndShowLengthsAndDivide() {
	Number number1, number2; //Definicja obiektów liczb

	number1 = fabricateNumber("Prosze wprowadzic pierwsza liczbe"); //Wywołanie funkcji fabrykującej obiekt z argumentem będącym komendą dla użytkownika.
	number2 = fabricateNumber("Prosze wprowadzic druga liczbe");

	cout << endl << "Pierwsza liczba jest: " + getOutputStringFromLength(number1.getLength()); //Wyświetlenie długości liczby zgodnie z podanym formatem używając do tego wcześniej zdefiniowanych metod i funkcji.
	cout << endl << "Druga liczba jest: " + getOutputStringFromLength(number2.getLength());

	try { //Próba wykonania funkcji wewnątrz
		cout << endl << "Wynik dzielenia: " << divideFirstByDoubleSecond(number1.toInt(), number2.toInt()); //Wyświetlenie wyniku dzielenia
	} catch(string exception) { //Przechwycenie ewentualnego wyjątku
		cout << endl << exception; //Wyświetlenie wyjątku
	}
	cout << endl; //Wyświetlenie znaku końca linii.
}

int main()
{
	collectPersonDataAndShowInformations(); //Wywołanie funkcji realizującej zadania 1-3
	collectTwoNumbersAndShowLengthsAndDivide(); //Wywołanie funkcji realizującej zadania 4-6

	return 0;
}

/*
* Metoda ustawiająca prywatną zmienną name na warotść podaną w atrybucie
*/
void Person::setName(string name) {
	this->name = name;
}

/*
* Metoda ustawiająca prywatną zmienną gender na warotść podaną w atrybucie.
*/
void Person::setGender(char gender) {
	gender = tolower(gender); //ustawienie zawsze małej litery
	if(gender == 'm' || gender == 'k') { //zmiana wartości tylko jeśli argument jest poprawny
		this->gender = gender; //zmiana wartości prywatnej
	}
}

/*
* Metoda wyświetlająca dane zgodnie z podanym w specyfikacji formatem
*/
string Person::sayHello() {
	map<char, string> phrases; //definicja mapy fraz
	phrases['m'] = "Pana"; //ustawianie wartości mapy.
	phrases['k'] = "Pania";

	return getVocativeOfSelfName() + ", witam " + phrases[gender]; //Zwrócenie obiektu string z wartością zgodną z wyjściem ze specyfikacji.
}

/*
* Metoda znajdująca płeć na podstawie ostatniej litery imienia.
*/
char Person::findGenderByName() {
	char lastChar; //definicja zmiennej będącej reprezentacją ostatniej litery imienia.
	lastChar = *name.rbegin(); //przypisanie do zmiennej lastChar wskaźnika na ostatnią literę imienia.
	if(lastChar == 'a') { //zwrócenie 'k' lub 'm' w zależności od tego czy ostatnia litera ma wartość a czy nie.
		return 'k';
	} else {
		return 'm';
	}
}

/*
* Metoda zwracająca wołacz wybranego polskiego imienia. Implementacja prostego algorytmu do znajdowania wołacza imienia.
*/
string Person::getVocativeOfSelfName() {
	map<char, string> transformations; //definicja mapy ostatnich liter imienia jako klucz i przekształceniem końcówki jako wartość.
	transformations['a'] = "o";
	transformations['b'] = "bie";
	transformations['c'] = "cu";
	transformations['d'] = "dzie";
	transformations['e'] = "e";
	transformations['f'] = "fie";
	transformations['g'] = "gu";
	transformations['h'] = "hu";
	transformations['i'] = "iu";
	transformations['j'] = "ju";
	transformations['k'] = "ku";
	transformations['l'] = "lu";
	transformations['m'] = "mie";
	transformations['n'] = "nie";
	transformations['o'] = "o";
	transformations['p'] = "pie";
	transformations['r'] = "rze";
	transformations['s'] = "sie";
	transformations['t'] = "cie";
	transformations['u'] = "u";
	transformations['w'] = "wie";
	transformations['y'] = "y";
	transformations['z'] = "zu";

	return name.substr(0, name.size()-1) + transformations[*name.rbegin()]; //zwrócenie przekształconej wersji imienia.
}

/*
* Metoda ustawiająca prywatną zmienną number na warotść podaną w atrybucie.
* @param number {int} - numer;
*/
void Number::set(int number) {
	this->number = number;
}

/*
* Metoda zwracająca prywatną właściwość number.
*/
int Number::toInt() {
	return number;
}

/*
* Metoda zwracająca długość prywatnej właściwości number.
*/
int Number::getLength() {
	if(number == 0) { //Sprawdzenie czy numer jest równy 0
		return 1; //Jeśli tak, zwrócenie 1
	}

	if((int) abs(number) == 1) { //Sprawdzenie czy wartość bezwzględna jest równa 1
		return 1; //Jeśli tak, zwrócenie 1
	}

	return (int) ceil(log10(abs(number) + 1)); //Zwrócenie zmiennej typu int jako wyniku zaokrąglenia w górę logarytmu dziesiętnego z wartości bezwzględnej prywatnej właściwości number.
}

//Przeciążenie operatora wyjścia strumienia.
ostream & operator<< (ostream &output, const Number &n) {
	return output << n.number; //zwrócenie wartości prywatnej właściwości number poprzez skorzystanie z domyślnego wyjścia strumienia dla int.
}

//Przeciążenie operatora wejścia strumienia.
istream & operator>> (istream &input, Number &n) {
	return input >> n.number; //przypisanie wartości do prywatnej właściwości number poprzez skorzystanie z domyślnego wejścia strumienia dla int.
}
